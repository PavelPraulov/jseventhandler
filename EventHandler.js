'use strict';

class FuncHolder {
	constructor(id, func, context, priority, eventNameIndex) {
		this.id   = id;
		this.func = func;
		this.context  = context;
		this.priority = priority;
        this.eventNameIndex = eventNameIndex;
	}
}

export default
class EventHandler {
	constructor() {
		this._subscriptionsNormal = [];
		this._subscriptionsBefore = [];
		this._subscriptionsAfter  = [];
		this._eventNames = [];
	}

    subscribe(eventName, callback, context, priority) {
        let resultPriority = priority || EventHandler.HIGH_PRIORITY;
        // if in array of events names doesn't exists such event name, put it otherwise get it index.
        let eventNameRegisterIndex = (this._eventNames.indexOf(eventName) > -1)
            ? this._eventNames.indexOf(eventName)
            : this._eventNames.push(eventName) - 1;
        let subscription   = new FuncHolder(eventName, callback, context, resultPriority, eventNameRegisterIndex);
        switch (resultPriority) {
            case EventHandler.HIGH_PRIORITY   : this._subscriptionsBefore.push(subscription);
                break;
            case EventHandler.NORMAL_PRIORITY : this._subscriptionsNormal.push(subscription);
                break;
            case EventHandler.LOW_PRIORITY    : this._subscriptionsAfter.push(subscription);
                break;
        }
	}

	// Try to call subscriptions by fired Event
    dispatchEvent(eventName, eventContext) {
        let eventNameRegisterIndex = this._eventNames.indexOf(eventName);
        if (eventNameRegisterIndex > -1) {
            this.dispatchEventByEventNameIndex(eventNameRegisterIndex, eventContext, this._subscriptionsBefore);
            this.dispatchEventByEventNameIndex(eventNameRegisterIndex, eventContext, this._subscriptionsNormal);
            this.dispatchEventByEventNameIndex(eventNameRegisterIndex, eventContext, this._subscriptionsAfter);
        }
	}

    dispatchEventIn(eventName, eventContext, array) {
        for (var it = 0; it < array.length; it++) {
            if (array[it].id === eventName) {
                if (!array[it].context) {
                    array[it].func();
                } else {
                    array[it].func.call(array[it].context, eventContext);
                }
            }
        }
    }

    dispatchEventByEventNameIndex(eventNameIndex, eventContext, array) {
        array.forEach(function(item) {
            if (item.eventNameIndex === eventNameIndex) {
                callFromHolder(item, eventContext);
            }
        });
    }

    callFromHolder(funcHolder, eventContext) {
        if (!funcHolder.context) {
            funcHolder.func();
        } else {
            funcHolder.func.call(funcHolder.context, eventContext);
        }
    }
}

EventHandler.HIGH_PRIORITY = 100;
EventHandler.NORMAL_PRIORITY = 200;
EventHandler.LOW_PRIORITY = 300;